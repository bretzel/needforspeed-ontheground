package game.model;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Car {

    private boolean isOn;
    private float speed;
    private String brand;
    private Model model;
    private float maxSpeed;
    private Integer posX;
    private Integer posY;
    private Integer dX;
    private Integer dY;
    private BufferedImage bodyImage;


    public Car(Model model) {
        this.model = model;
        this.isOn = false;
        this.speed = 0;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }

    public float getSpeed() {
        return speed;
    }

    public Integer getPosX() {
        return posX;
    }

    public void setPosX(Integer posX) {
        this.posX = posX;
    }

    public Integer getPosY() {
        return posY;
    }

    public void setPosY(Integer posY) {
        this.posY = posY;
    }

    public Integer getdX() {
        return dX;
    }

    public void setdX(Integer dX) {
        this.dX = dX;
    }

    public Integer getdY() {
        return dY;
    }

    public void setdY(Integer dY) {
        this.dY = dY;
    }

    public BufferedImage getBodyImage() {
        return bodyImage;
    }

    public void setBodyImage(BufferedImage bodyImage) {
        this.bodyImage = bodyImage;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void start() {
        this.setOn(true);
    }

    public void stop() {
        this.setOn(false);
    }

    public void goForward() {
        ;
    }

    public void goBackWard() {
        ;
    }

    public void turnLeft() {
        ;
    }

    public void turnRight() {
        ;
    }
}
