package game.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

public class Ferrari extends Car {

    public enum Model implements game.model.Model {
        LaFerrari,
        Testarossa;
    }

    public Ferrari(game.model.Model model) {
        super(model);
        String path = "res/";
        if (model == Model.LaFerrari) {
            this.setMaxSpeed(380);
            path += "ferrari.png";
        } else if (model == Model.Testarossa) {
            this.setMaxSpeed(360);
            path += "testarossa.png";
        }
        try {
            this.setBodyImage(ImageIO.read(new File(path)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getModels() {
        ArrayList<String> models = new ArrayList<>();
        for (Model model : Model.values()) {
            models.add(model.toString());
        }
        return models;
    }
}

