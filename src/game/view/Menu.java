package game.view;

import game.controller.MenuCtrl;
import game.model.Model;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Menu extends JFrame {

    public Menu() {
        MenuCtrl menuCtrl = new MenuCtrl(this);
        this.setTitle("Need For Speed On The Ground");
        this.setSize(800, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel carSelectorPanel = new JPanel();
        JLabel carSelection = new JLabel("Choose your car");
        String[] cars = new String[] {"Ferrari", "Lambo", "Ford"};
        JComboBox<String> carsList = new JComboBox<>(cars);
        carSelectorPanel.add(carSelection);
        carSelectorPanel.add(carsList);
        JButton modelSelection = new JButton("Select model");
        modelSelection.addActionListener(e -> {
            JComboBox<String> modelsList = new JComboBox<>(menuCtrl.getModelsByBrand((String)carsList.getSelectedItem()));
            carSelectorPanel.add(modelsList);
            modelSelection.setVisible(false);
            JButton start = new JButton("Start Game !");
            start.addActionListener(ev -> {
                menuCtrl.startGame((String)modelsList.getSelectedItem());
            });
            carSelectorPanel.add(start);
            carSelectorPanel.revalidate();
        });

        carSelectorPanel.add(modelSelection);
        this.add(carSelectorPanel);
        this.setVisible(true);
    }
}
