package game.view;

import game.controller.CircuitCtrl;
import game.model.Car;

import javax.swing.*;

public class Circuit extends JFrame {
    private Car car;
    private JPanel panel = new JPanel();
    public Circuit() {
        this.setTitle("Need For Speed On The Ground");
        this.setSize(800, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.add(this.panel);
        CircuitCtrl circuit = new CircuitCtrl(this);
    }

    public void addCarOnCircuit(Car car) {
        this.car = car;
        JLabel carBody = new JLabel(new ImageIcon(this.car.getBodyImage()));
        this.panel.add(carBody);
        this.panel.revalidate();
    }
}
