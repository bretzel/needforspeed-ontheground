package game.controller;

import game.model.Ferrari;
import game.model.Model;
import game.view.Circuit;

import javax.swing.*;

public class MenuCtrl {

    private JFrame menu;

    public MenuCtrl(JFrame menu) {
        this.menu = menu;
    }

    public String[] getModelsByBrand(String brand) {
        switch (brand) {
            case "Ferrari":
                return Ferrari.getModels().toArray(new String[0]);
            case "Lambo":
                break;
            case "Ford":
                break;
        }
        return new String[] {"pute"};
    }

    public void startGame(String model) {
        Model mdl = null;
        switch (model) {
            case "LaFerrari":
                mdl = Ferrari.Model.LaFerrari;
                break;
            case "Testarossa":
                mdl = Ferrari.Model.Testarossa;
                break;
        }
        menu.setVisible(false);
        menu.dispose();
        CircuitCtrl.startCircuit(mdl);
    }
}
