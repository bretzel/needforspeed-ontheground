package game.controller;

import game.model.Car;
import game.model.Ferrari;
import game.model.Model;
import game.view.Circuit;

import javax.swing.*;

public class CircuitCtrl {

    private JFrame circuit;

    public CircuitCtrl(JFrame circuit) {
        this.circuit = circuit;
    }

    protected static void startCircuit(Model model) {
        Circuit circuit = new Circuit();
        Ferrari car = new Ferrari(model);
        System.out.println(car.getModel());
        circuit.addCarOnCircuit(car);

    }
}
